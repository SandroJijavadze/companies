package main

import server "companies/api"

func main() {

	s := server.NewServer()
	s.Initialize()
	s.Run(":8080")
}
