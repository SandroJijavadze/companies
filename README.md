#  Companies

### Building & running
` docker-compose up`

Api will be served on 8080 port according to docker-compose.

## Endpoints

Note they are prefixed with `/api/v1/`

```
	s.Router.HandleFunc("/companies", controllers.GetMany).Methods("GET")
	s.Router.HandleFunc("/companies/{id}", controllers.Get).Methods("GET")
	s.Router.HandleFunc("/companies", middlewares.IPCheck(middlewares.JWTAuth(controllers.Create), "CY")).Methods("POST")
	s.Router.HandleFunc("/companies/{id}", middlewares.IPCheck(middlewares.JWTAuth(controllers.Update), "CY")).Methods("PUT")
	s.Router.HandleFunc("/companies/{id}", middlewares.IPCheck(middlewares.JWTAuth(controllers.Delete), "CY")).Methods("DELETE")

	s.Router.HandleFunc("/auth", controllers.Auth).Methods("POST")
```

Auth endpoint receives user and password as a post form. Currently it is hardcoded to `admin:admin`, returns JWT token.


What is missing: 
Right now implementation for PUT is missing. And filtration by query parameters on `/companies` endpoint.
Tests are currently only created for create and delete methods. 
