package controllers

import (
	"companies/api/middlewares"
	"companies/api/responses"
	"errors"
	"net/http"
)

func Auth(w http.ResponseWriter, r *http.Request) {
	// Let's hardcode user and password for now
	err := r.ParseForm()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not parse request"))
		return
	}

	user := r.PostForm.Get("user")
	password := r.PostForm.Get("password")

	if user == "admin" && password == "admin" {
		token, err := middlewares.CreateToken()
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not create token"))
			return
		}
		responses.JSON(w, http.StatusOK, token)
		return
	} else {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("invalid credentials"))
		return
	}
}
