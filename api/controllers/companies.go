package controllers

import (
	"companies/api/models"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"net/http"
	"strconv"
)
import "companies/api/responses"

func Create(w http.ResponseWriter, r *http.Request) {
	company := new(models.Company)

	body, err := io.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	err = json.Unmarshal(body, company)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	c, err := models.CreateCompany(company)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not process"))
		return
	}

	responses.JSON(w, http.StatusCreated, c.ID)
}

func Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	cid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	company, err := models.GetCompany(cid)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("could not find company"))
		return
	}

	responses.JSON(w, http.StatusOK, company)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	cid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	_, err = models.DeleteCompany(cid)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("could not delete company"))
		return
	}

	w.Header().Set("Entity", fmt.Sprintf("%d", cid))
	responses.JSON(w, http.StatusNoContent, "")

}

func Update(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, 501, "Not Implemented")
}

func GetMany(w http.ResponseWriter, r *http.Request) {
	companies, err := models.GetCompanies()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not process"))
		return
	}

	responses.JSON(w, 200, companies)
}
