package tests

import (
	"companies/api/models"
	"github.com/stretchr/testify/assert"
)

func (t *SuiteTest) TestCreateCompany() {
	_, err := models.CreateCompany(&models.Company{
		Name:    "CompanyA",
		Code:    "CY",
		Country: "Cyprus",
		Website: "test.gr",
		Phone:   "+357599001122",
	})
	assert.NoError(t.T(), err)

	_, err = models.CreateCompany(&models.Company{
		Name:    "CompanyB",
		Code:    "GE",
		Country: "Georgia",
		Website: "testb.com",
		Phone:   "+995599001123",
	})
	assert.NoError(t.T(), err)

	_, err = models.CreateCompany(&models.Company{
		Name:    "CompanyC",
		Code:    "h",
		Country: "i",
		Website: "testb.com",
		Phone:   "+995599001124",
	})
	assert.NoError(t.T(), err)
}

func (t *SuiteTest) TestDeleteCompany() {
	c, err := models.CreateCompany(&models.Company{
		Name:    "CompanyA",
		Code:    "CY",
		Country: "Cyprus",
		Website: "test.gr",
		Phone:   "+357599001122",
	})
	assert.NoError(t.T(), err)

	count, err := models.DeleteCompany(c.ID)
	assert.NoError(t.T(), err)
	assert.Equal(t.T(), count, int64(1))

}
