package tests

import (
	"companies/api/config"
	"companies/api/models"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"log"
	"os"
	"testing"
)

type SuiteTest struct {
	suite.Suite
	db *gorm.DB
}

func TestSuite(t *testing.T) {
	err := godotenv.Load("../../.env")
	if err != nil {
		log.Println("could not load .env file")
	}
	suite.Run(t, new(SuiteTest))
}

func getModels() []interface{} {
	return []interface{}{&models.Company{}}
}

// Setup db value
func (t *SuiteTest) SetupSuite() {
	config.ConnectGorm(os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_NAME"), os.Getenv("DB_PASS"))
	t.db = config.GetDB()

	// Migrate Table
	for _, val := range getModels() {
		err := t.db.AutoMigrate(val)
		if err != nil {
			t.Errorf(err, "could not migrate table")
		}
	}
}

// Run After All Test Done
func (t *SuiteTest) TearDownSuite() {
	sqlDB, _ := t.db.DB()
	defer sqlDB.Close()

	// Drop Table
	for _, val := range getModels() {
		err := t.db.Migrator().DropTable(val)
		if err != nil {
			t.Errorf(err, "could not drop table")
		}
	}
}
