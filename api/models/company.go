package models

import (
	"companies/api/config"
	"errors"
	"gorm.io/gorm"
)

type Company struct {
	ID      uint64 `json:"id" gorm:"primary_key;auto_increment;not_null"`
	Name    string `json:"name" gorm:"type:varchar(100)" schema:"name"`
	Code    string `json:"code" gorm:"type:varchar(3)" schema:"code"`
	Country string `json:"country" gorm:"type:varchar(20)" schema:"country"`
	Website string `json:"website" gorm:"type:varchar(50)" schema:"website"`
	Phone   string `json:"phone" gorm:"type:varchar(20)" schema:"phone"`
}

type CompanyFilter struct {
	Company
	Offset int64  `schema:"offset"`
	Limit  int64  `schema:"limit"`
	SortBy string `schema:"sortby"`
	Asc    bool   `schema:"asc"`
}

func CreateCompany(c *Company) (*Company, error) {
	db := config.GetDB()

	err := db.Debug().Model(&Company{}).Create(&c).Error
	if err != nil {
		return &Company{}, err
	}
	return c, nil
}

func GetCompany(cid uint64) (*Company, error) {
	db := config.GetDB()

	var c Company
	err := db.Debug().Model(&Company{}).Where("id = ?", cid).Take(&c).Error
	if err != nil {
		return &Company{}, err
	}

	return &c, nil
}

func DeleteCompany(cid uint64) (int64, error) {
	db := config.GetDB()

	db = db.Debug().Model(&Company{}).Where("id = ?", cid).Take(&Company{}).Delete(&Company{})

	if db.Error != nil {
		if db.Error == gorm.ErrRecordNotFound {
			return 0, errors.New("Company not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

func GetCompanies() ([]*Company, error) {
	db := config.GetDB()

	var companies []*Company

	// @TODO ADD FILTER USAGE HERE

	if err := db.Model(companies).Find(&companies).Error; err != nil {
		return nil, err
	}

	return companies, nil
}
