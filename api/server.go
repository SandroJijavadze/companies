package server

import (
	"companies/api/config"
	"companies/api/models"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gorm.io/gorm"
	"log"
	"net/http"
	"os"
)

type Server struct {
	Router *mux.Router
	DB     *gorm.DB
}

func NewServer() Server {
	return Server{}
}
func (s *Server) Initialize() {
	err := godotenv.Load()
	if err != nil {
		log.Println("could not load .env file")
	}

	db := config.ConnectGorm(os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_NAME"), os.Getenv("DB_PASSWORD"))
	err = db.AutoMigrate(models.Company{})
	if err != nil {
		log.Fatal(err.Error())
	}

	s.DB = db
	router := mux.NewRouter()

	apiV1 := router.PathPrefix("/api/v1").Subrouter()
	s.Router = apiV1
	s.initializeRoutes()
}

func (s *Server) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, s.Router))
}
