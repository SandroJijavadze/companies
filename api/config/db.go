package config

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	db *gorm.DB
)

func GetDB() *gorm.DB {
	return db
}

func ConnectGorm(host, port, user, database, password string) *gorm.DB {
	connString := fmt.Sprintf("host=%s user=%s password=%s port=%s sslmode=disable",
		host, user, password, port,
	)
	if database != "" {
		connString = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable", host, user, password, database, port)
	}

	var err error
	db, err = gorm.Open(postgres.Open(connString), &gorm.Config{})

	if err != nil {
		fmt.Println(err)
		panic("could not connect to database")
	}
	return db
}
