package middlewares

import (
	"companies/api/responses"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
)

type ipResponse struct {
	Country string `json:"country_code"`
	City    string `json:"city"`
}

func IPCheck(next http.HandlerFunc, country_code string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		baseURL := "https://ipapi.co/%s/json"
		ip, err := getIP(r)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not authorize based on user ip(1)"))
			return
		}

		resp, err := http.Get(fmt.Sprintf(baseURL, ip))
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not authorize based on user ip(2)"))
			return
		}

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not authorize based on user ip(3)"))
			return
		}

		var ipres ipResponse
		err = json.Unmarshal(body, &ipres)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, errors.New("could not authorize based on user ip(4)"))
			return
		}

		if ipres.Country != country_code {
			responses.ERROR(w, http.StatusUnprocessableEntity, fmt.Errorf("expected country: CY, received: %s for ip %s", ipres.Country, ip))
			return
		}

		next(w, r)
	}

}

func getIP(r *http.Request) (net.IP, error) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return nil, fmt.Errorf("userip: %q is not IP:port", r.RemoteAddr)
	}

	userIP := net.ParseIP(ip)
	if userIP == nil {
		return nil, fmt.Errorf("userip: %q is not IP:port", r.RemoteAddr)

	}

	return userIP, nil
}
