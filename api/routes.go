package server

import (
	"companies/api/controllers"
	"companies/api/middlewares"
)

func (s *Server) initializeRoutes() {
	s.Router.HandleFunc("/companies", controllers.GetMany).Methods("GET")
	s.Router.HandleFunc("/companies/{id}", controllers.Get).Methods("GET")
	s.Router.HandleFunc("/companies", middlewares.IPCheck(middlewares.JWTAuth(controllers.Create), "CY")).Methods("POST")
	s.Router.HandleFunc("/companies/{id}", middlewares.IPCheck(middlewares.JWTAuth(controllers.Update), "CY")).Methods("PUT")
	s.Router.HandleFunc("/companies/{id}", middlewares.IPCheck(middlewares.JWTAuth(controllers.Delete), "CY")).Methods("DELETE")

	s.Router.HandleFunc("/auth", controllers.Auth).Methods("POST")

}
